/*
** my.h for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/include
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 14:58:05 2015 remy grandry
** Last update Sun May 24 21:14:25 2015 grandr_r
*/

#ifndef MY_H_
# define MY_H_
# define PROMPT "$>"
#define _GNU_SOURCE
#include <sys/types.h>
#include <unistd.h>

typedef struct	s_42
{
  struct s_42	*prev;
  struct s_42	*next;
  char		*buff;
}		t_42;

typedef struct	s_sh
{
  int		pos;
  char		**msg;
  char		*current_pwd;
  char		**cur_prompt;
  char		*oldpwd;
  char		**all_cmd;
  char		**my_env;
  char		**epur_cmd;
  char		**my_path;
  char		**new;
  char		*true_cmd;
  int		return_value;
  pid_t		id;
  int		pipefd[2];
  int		fd;
  char		**p_cmd;
  int		termcap;
  int		safe;
}		t_sh;

t_42	*init_list();
void	init_all(t_42 *cmd, t_sh *sh, char **env);
void    display_list(t_42 *list);
void    remove_elem(t_42 *root);
void    del_first_elem(t_42 *root);
void    add_next(t_42 *root, char *str);
void    add_prev(t_42 *root, char *str);
char    *get_next_line(int fd, t_42 *sh, t_sh *sh2);
char    *epur_str(char *str, char sep);
int     match(char *s1, char *s2);
int	my_exit(t_42 *sh, t_sh *sh2, char *s);
int	exit_ctrld(t_42 *sh, t_sh *sh2, char *s);
void	my_putchar(char c, int out);
void	my_putstr(char *str, int out);
int	my_strlen(char *str);
int	canon_on(t_sh *sh);
int	canon_off(t_sh *sh);
char    *arrow_control(char *s, char *buf, t_42 **sh, t_sh *sh2);
void	disp_cmd(char *cmd, char *s, t_sh *sh);
void	remove_letter(char *s, int pos);
char    *backspace(char *s, t_sh *sh2);
char    *delete_key(char *s, t_sh *sh2);
int     my_strcmp(char *s1, char *s2);
char	*my_clrscreen(char *s, char *tmp);
int     char_is_printable(char c);
char	*my_strdup(char *str);
char	*my_strdup2(char *str, char *s2);
int     my_listlen(t_42 *list);
int     my_cd(t_42 *cmd, t_sh *sh, char *str);
int     my_builtins(t_42 **cmd, t_sh *sh, char *str, char **tmp);
void    my_free_wordtab(char **wt);
char    *my_strcatc(char *s1, char i);
char    *my_strcat(char *s1, char *s2);
int	function_error(char *func, char *param);
char	**my_str_to_wordtab(char *str, int i, char sep);
void	my_free_wordtab(char **wt);
int     my_cd(t_42 *cmd, t_sh *sh, char *str);
int     char_is_instr(char c, char *s);
int	my_env_copy(t_sh * sh, char **env);
char	*my_getenv(char **env, char *str);
void	my_uptdate_path(t_sh *sh);
int	my_env_print(char **env);
int	my_echo(t_42 *cmd, t_sh *sh, char *str);
void	my_print_backslash(char *str);
void	my_print_echo(char *str, char **env);
int	my_strncmp(char *s1, char *s2, int n);
void	my_shell_exec(t_42 **cmd, t_sh *sh, char *s);
int	exec_error(char *func);
char	*fill_path(t_sh *sh, char *name);
char    *my_realloc(char *str, char *buffer);
int     only_chars(char *s, char sep, char sep2, t_sh *sh);
int	my_arrlen(char **cmd);
int	disp_prompt(t_sh *sh);
void	create_prompt(char **env, int n, char *s);
void	disp_prompt_ctrlc();
int	my_export(char *str, t_sh *sh);
int	my_unsetenv(char *str, t_sh *sh);
int	my_history(char *str, t_sh *sh, t_42 **cmd);
int	hist_mark(char *str, t_sh *sh, t_42 **cmd);
void	get_true_cmd(char *str, t_42 *sh);
void	my_shell_logical(t_42 **cmd, t_sh *sh, char *s);
int     my_shell_and(t_42 **cmd, t_sh *sh, char *str);
int     my_shell_or(t_42 **cmd, t_sh *sh, char *str);
int     is_redir(char *s, t_42 **cmd, t_sh *sh);
int     redir_sleft(char *s, t_42 **cmd, t_sh *sh, char **tmp);
int     redir_dleft(char *s, t_42 **cmd, t_sh *sh, char **tmp);
int     redir_sright(char *s, t_42 **cmd, t_sh *sh, char **tmp);
int     redir_dright(char *s, t_42 **cmd, t_sh *sh, char **tmp);
char    *convert_cmd(char *s, t_sh *sh, int i, int j);
char	*my_reallocc(char *s, char c, int pos);
char	*get_line(int fd);
int	my_run_pipe(char *s, t_42 **cmd, t_sh *sh, char **tmp);
void    add_to_env(int ret, t_sh *sh);
void    get_return_value(pid_t id, t_sh *sh);
void	my_free_wordtab2(char ***wt);
int	return_error(char *s, char **tmp);
int	only_char(char *s, char sep);
void	my_free_wordtab3(char **wt, char **wt2);
int	function_error2(char *func, char *param, char **tmp, char **tmp2);
int     pipe_is_redir(char *s, t_sh *sh);
void    my_logical_pipe(t_42 **cmd, t_sh *sh, char *tmp);
int     shexec_pipe(char **tmp, char **env, t_sh *sh);
char	*my_unwordtab(char **wt);
void    my_logical_pipe_rec(t_sh *sh, char *tmp);
int	test_dir(char *p);
int	command_error(char *s, t_sh *sh, char **tmp);
char	*my_strcat3(char *d, char *s);
int	my_setenv(char *s, t_sh *sh);
void	add_ls_alias(char **wt);
int	my_getnbr(char *s);
char	*tab_key(char *s, t_sh *sh, int i, char *s2);
char    *func_name(char *s, int mode);
int	shexec(char *s, char **wt, char **env, t_sh *sh);

#endif /* !MY_H_ */
