##
## Makefile for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
##
## Made by grandr_r
## Login   <grandr_r@epitech.net>
##
## Started on  Wed May  6 15:50:19 2015 grandr_r
## Last update Sun May 24 20:31:15 2015 grandr_r
## Started on  Wed Jan 14 20:18:45 2015 Remy GRANDRY
## Last update Tue Apr 28 17:26:20 2015 adrien monceyron
##

VAR=		src/main.c \
		src/init_list.c \
		src/list.c \
		src/match.c \
		src/get_next_line.c \
		src/my_exit.c \
		src/my_string.c \
		src/my_getenv.c \
		src/my_str_to_wordtab.c \
		src/canon.c \
		src/arrow_control.c \
		src/epur_str.c \
		src/other_key.c \
		src/my_strdup.c \
		src/my_strcat.c \
		src/my_builtins.c \
		src/my_free.c \
		src/error.c \
		src/my_cd.c \
		src/my_env_copy.c \
		src/my_path.c \
		src/my_echo.c \
		src/print_format.c \
		src/my_string2.c \
		src/my_shell_exec.c \
		src/disp_prompt.c \
		src/my_export.c \
		src/my_unsetenv.c \
		src/my_history.c \
		src/logical.c \
		src/my_redirections.c \
		src/convert_cmd.c \
		src/my_pipe.c \
		src/my_redir_pipe.c \
		src/my_exec_pipe.c \
		src/my_setenv.c \
		src/myls.c \
		src/get_line.c

OBJ=		$(VAR:.c=.o)

CC=		gcc

CFLAGS +=	-Wall -Werror -pedantic -I./include

NAME=		42sh

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CC) -o $(NAME) $(OBJ)

clean:
		rm -f $(OBJ)

fclean:
		rm -f src/*~ $(OBJ) $(NAME)

re:		fclean all
