/*
** init_list.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/src
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 15:10:53 2015 remy grandry
** Last update Sun May 24 21:08:42 2015 grandr_r
*/

#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <my.h>

int	my_listlen(t_42 *list)
{
  int	i;
  t_42	*root;

  i = 0;
  root = list->next;
  while (root != list)
    {
      root = root->next;
      i++;
    }
  return (i);
}

t_42	*init_list()
{
  t_42	*list;

  if ((list = malloc(sizeof(*list))) == NULL)
    exit(-1);
  list->prev = list;
  list->next = list;
  list->buff = NULL;
  return (list);
}

void	init_msg(t_sh *sh, int i)
{
  if ((sh->msg = malloc(sizeof(char *) * 35)) == NULL)
    exit(-1);
  while (++i < 34)
    sh->msg[i] = my_strdup("Unknow signal");
  sh->msg[SIGHUP] = my_strdup2("Hangup", sh->msg[SIGHUP]);
  sh->msg[SIGINT] = my_strdup2("Interrupt", sh->msg[SIGINT]);
  sh->msg[SIGILL] = my_strdup2("Illegal Instruction", sh->msg[SIGILL]);
  sh->msg[SIGTRAP] = my_strdup2("Trace Trap", sh->msg[SIGTRAP]);
  sh->msg[SIGABRT] = my_strdup2("Abort", sh->msg[SIGABRT]);
  sh->msg[SIGFPE] = my_strdup2("Arithmetic Exception", sh->msg[SIGFPE]);
  sh->msg[SIGKILL] = my_strdup2("Killed", sh->msg[SIGKILL]);
  sh->msg[SIGBUS] = my_strdup2("Bus Error", sh->msg[SIGBUS]);
  sh->msg[SIGSEGV] = my_strdup2("Segmentation Fault", sh->msg[SIGSEGV]);
  sh->msg[SIGSYS] = my_strdup2("Bad System Call", sh->msg[SIGSYS]);
  sh->msg[SIGPIPE] = my_strdup2("Broken Pipe", sh->msg[SIGPIPE]);
  sh->msg[SIGALRM] = my_strdup2("Alarm", sh->msg[SIGALRM]);
  sh->msg[SIGPWR] = my_strdup2("suspended (signal)", sh->msg[SIGPWR]);
  sh->msg[SIGTSTP] = my_strdup2("cpu limit exceeded", sh->msg[SIGTSTP]);
  sh->msg[SIGCONT] = my_strdup2("file size limit exceed", sh->msg[SIGCONT]);
  sh->msg[SIGTTIN] = my_strdup2("virtual time alarm", sh->msg[SIGTTIN]);
  sh->msg[SIGTTOU] = my_strdup2("profile signal", sh->msg[SIGTTOU]);
  sh->msg[SIGPROF] = my_strdup2("Bad System Call", sh->msg[SIGPROF]);
  sh->msg[SIGXCPU] = my_strdup2("power fail", sh->msg[SIGXCPU]);
  sh->msg[SIGXFSZ] = my_strdup2("invalid system call", sh->msg[SIGXFSZ]);
  sh->msg[34] = NULL;
}

void	my_signal()
{
  my_putstr("", 2);
}

void	init_all(t_42 *cmd, t_sh *sh, char **env)
{
  signal(SIGQUIT, my_signal);
  signal(SIGINT, disp_prompt_ctrlc);
  signal(SIGTSTP, my_signal);
  sh->pos = -1;
  sh->oldpwd = NULL;
  sh->current_pwd = NULL;
  sh->all_cmd = NULL;
  sh->epur_cmd = NULL;
  sh->my_path = NULL;
  sh->cur_prompt = NULL;
  sh->new = NULL;
  sh->true_cmd = NULL;
  init_msg(sh, -1);
  sh->return_value = 0;
  canon_on(sh);
  create_prompt(env, 0, NULL);
  my_export("export ?=0", sh);
}
