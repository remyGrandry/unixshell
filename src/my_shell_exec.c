/*
** my_shell_exec.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Fri May  8 19:30:44 2015 grandr_r
** Last update Wed Jun  3 09:40:08 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <my.h>

void	add_to_env(int ret, t_sh *sh)
{
  char	buff[10];
  char	*s;

  sprintf(buff, "%d", ret);
  s = my_strdup("export ?=");
  s = my_realloc(s, buff);
  my_export(s, sh);
  free(s);
}

void	get_return_value(pid_t id, t_sh *sh)
{
  int	test;

  test = 0;
  waitpid(id, &test, 0);
  if (WIFSIGNALED(test))
    {
      my_putstr(sh->msg[WTERMSIG(test)], 2);
      WCOREDUMP(test) ? my_putstr(" (core dumped)", 2) : my_putstr("", 2);
      my_putchar('\n', 2);
    }
  if (WIFEXITED(test))
    sh->return_value = WEXITSTATUS(test);
  wait(&id);
  add_to_env(sh->return_value, sh);
}

int	shexec(char *str, char **tmp, char **env, t_sh *sh)
{
  pid_t	id;

  if (access(str, X_OK) == -1)
    {
      exec_error(str);
      return (-1);
    }
  if ((id = fork()) == -1)
    {
      my_putstr(strerror(errno), 2);
      my_putchar('\n', 2);
      return (-1);
    }
  else if (id == 0)
    {
      if (execve(str, tmp, env) == -1)
	exec_error(str);
      exit(0);
    }
  else
    get_return_value(id, sh);
  return (0);
}

void	my_shell_exec(t_42 **cmd, t_sh *sh, char *str)
{
  char	**tmp;
  char	*t_p;

  t_p = NULL;
  if (str == NULL)
    return;
  if (my_builtins(cmd, sh, str, sh->epur_cmd) == 1)
    return;
  tmp = my_str_to_wordtab(str, 0, ' ');
  if (test_dir(tmp[0]) == 0)
    {
      command_error(tmp[0], sh, tmp);
      return;
    }
  if (access(tmp[0], F_OK) == 0)
    shexec(tmp[0], tmp, sh->my_env, sh);
  else if (match(tmp[0], "*/*") == 0 && (t_p = fill_path(sh, tmp[0])) != NULL)
    {
      shexec(t_p, tmp, sh->my_env, sh);
      free(t_p);
    }
  else
    command_error(tmp[0], sh, NULL);
  my_free_wordtab(tmp);
}

void	my_shell_logical(t_42 **cmd, t_sh *sh, char *str)
{
  if (match(str, "*||*") == 1)
    my_shell_or(cmd, sh, str);
  else if (match(str, "*&&*"))
    my_shell_and(cmd, sh, str);
  else if (is_redir(str, cmd, sh) == 1)
    return;
  else
    my_shell_exec(cmd, sh, str);
}
