/*
** my_str_to_wordtab2.c for my_str_to_wordtab in /Users/grandr/Epitech/Unix_Prog/PSU_2014_minishell1
**
** Made by Remy GRANDRY
** Login   <grandr@epitech.net>
**
** Started on  Thu Jan 15 17:13:14 2015 Remy GRANDRY
** Last update Sat May  9 14:33:11 2015 grandr_r
*/

#include <stdlib.h>
#include <stw.h>

char    **my_str_to_wordtab2(char *str, int cpt, char sep)
{
  t_stw	t;
  char  **tab;

  t.i = 0;
  while (str[t.i] == sep || str[t.i] == '\t')
    t.i++;
  t.len = t.i;
  while ((str[t.len] != sep) && (str[t.len] != '\t') && (str[t.len] != '\0'))
    t.len++;
  t.j = t.len;
  while (str[t.j] == sep || str[t.j] == '\t')
    t.j++;
  tab = (str[t.j] == '\0' ? malloc((cpt + 2) * sizeof(char *)) : NULL);
  if (str[t.j] == '\0')
    tab[cpt + 1] = NULL;
  else
    tab = my_str_to_wordtab(&str[t.len], cpt + 1, sep);
  tab[cpt] = malloc(((t.len - t.i) + 1) * sizeof(char));
  t.j = 0;
  while (t.i < t.len)
    tab[cpt][t.j++] = str[t.i++];
  tab[cpt][t.j] = '\0';
  return (tab);
}

char	**my_str_to_wordtab(char *str, int cpt, char sep)
{
  if (str == NULL)
    return (NULL);
  return (my_str_to_wordtab2(str, cpt, sep));
}
