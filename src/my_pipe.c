/*
** my_pipe.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May 20 11:39:50 2015 grandr_r
** Last update Tue Jun  2 19:03:06 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <my.h>

void     shexec_multi_pipe(t_sh *sh, int i, t_42 **cmd)
{
  while (sh->p_cmd[i] != NULL)
    {
      if (pipe(sh->pipefd) == -1)
	return;
      if ((sh->id = fork()) == -1)
	return;
      else if (sh->id == 0)
	{
	  if (dup2(sh->fd, 0) == -1)
	    exit(-1);
	  if (sh->p_cmd[i + 1] != NULL)
	    dup2(sh->pipefd[1], 1);
	  close(sh->pipefd[0]);
	  my_logical_pipe(cmd, sh, sh->p_cmd[i]);
	  exit(0);
	}
      else
	{
	  get_return_value(sh->id, sh);
	  close(sh->pipefd[1]);
	  sh->fd = sh->pipefd[0];
	  i++;
	}
    }
}

int	is_process_right(t_42 **cmd, t_sh *sh, char *str, int i)
{
  char	**tmp;
  char	*t_p;

  if (str == NULL || only_char(str, ' ') == 1)
    return (0);
  if (my_builtins(cmd, sh, str, sh->epur_cmd) == 1)
    return (0);
  tmp = my_str_to_wordtab(str, 0, ' ');
  if (test_dir(tmp[0]) == 0)
    return (command_error(tmp[0], sh, tmp));
  if (access(tmp[0], F_OK) == 0 && access(tmp[0], X_OK) == 0)
    sh->p_cmd[i] = my_strdup(str);
  else if (match(tmp[0], "*/*") == 0 &&
	   (t_p = fill_path(sh, tmp[0])) != NULL && access(t_p, X_OK) != -1)
    {
      tmp[0] = my_strdup2(t_p, tmp[0]);
      free(t_p);
      sh->p_cmd[i] = my_unwordtab(tmp);
    }
  else
    return (command_error(tmp[0], sh, tmp));
  my_free_wordtab(tmp);
  return (1);
}

int	my_run_pipe(char *s, t_42 **cmd, t_sh *sh, char **tmp)
{
  int	i;

  i = -1;
  sh->fd = 0;
  if (tmp[1] == NULL)
    return (return_error("Illegal use of |\n", tmp));
  if ((sh->p_cmd = malloc(sizeof(char *) * (my_arrlen(tmp) + 2))) == NULL)
    exit(-1);
  while (++i < my_arrlen(tmp))
    {
      if (is_process_right(cmd, sh, tmp[i], i) == 0)
	{
	  sh->p_cmd[i] = NULL;
	  my_free_wordtab(tmp);
	  my_free_wordtab(sh->p_cmd);
	  return (1);
	}
    }
  sh->p_cmd[i] = NULL;
  shexec_multi_pipe(sh, 0, cmd);
  my_free_wordtab(tmp);
  my_free_wordtab(sh->p_cmd);
  return (1);
}
