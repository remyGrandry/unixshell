/*
** logical.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Fri May 15 10:57:01 2015 grandr_r
** Last update Sat May 23 20:25:44 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

void	my_shell_logical_rec(t_42 **cmd, t_sh *sh, char *str, char **tmp)
{
  if (match(str, "*||*") == 1)
    my_shell_or(cmd, sh, str);
  else if (match(str, "*&&*"))
    my_shell_and(cmd, sh, str);
  else if (is_redir(str, cmd, sh) == 1)
    {
      free(str);
      return;
    }
  else
    my_shell_exec(cmd, sh, str);
  free(str);
}

char	**epur_wordtab(char *str, char **tmp, char c, char sep)
{
  char	**tmp2;
  int	i;

  tmp2 = my_str_to_wordtab(str, 0, c);
  i = 0;
  while (tmp[i])
    {
      free(tmp2[i]);
      tmp2[i] = epur_str(tmp[i], sep);
      i++;
    }
  my_free_wordtab(tmp);
  return (tmp2);
}

int	my_shell_and(t_42 **cmd, t_sh *sh, char *str)
{
  char  **tmp;
  char	**tmp2;
  int   i;

  i = 0;
  tmp = my_str_to_wordtab(str, 0, '&');
  tmp2 = epur_wordtab(str, tmp, '&', ' ');
  while (tmp2[i])
    {
      my_shell_logical_rec(cmd, sh, my_strdup(tmp2[i]), tmp2);
      if (match(my_getenv(sh->my_env, "?="), "0") == 0)
        {
	  my_free_wordtab(tmp2);
	  my_export("export ?=1", sh);
          return (1);
        }
      i++;
    }
  my_free_wordtab(tmp2);
  my_export("export ?=0", sh);
  return (0);
}

int	my_shell_or(t_42 **cmd, t_sh *sh, char *str)
{
  char  **tmp;
  char	**tmp2;
  int   i;

  i = 0;
  tmp = my_str_to_wordtab(str, 0, '|');
  tmp2 = epur_wordtab(str, tmp, '|', ' ');
  while (tmp2[i])
    {
      my_shell_logical_rec(cmd, sh, my_strdup(tmp2[i]), tmp2);
      if (match(my_getenv(sh->my_env, "?="), "0") == 1)
        {
	  my_free_wordtab(tmp2);
	  my_export("export ?=0", sh);
          return (1);
        }
      i++;
    }
  my_free_wordtab(tmp2);
  my_export("export ?=1", sh);
  return (0);
}
