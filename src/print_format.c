/*
** print_format.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sat May  9 10:23:30 2015 grandr_r
** Last update Sun May 24 18:25:24 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

int	find_sep(char *s, char sep)
{
  int	i;

  i = 0;
  while (s[i])
    {
      if (s[i] == sep)
	return (i);
      i++;
    }
  return (i);
}

void	my_print_echo(char *s, char **env)
{
  int	i;

  i = 0;
  while (s[i])
    {
      if (s[i] == '"')
	i++;
      else
	{
	  my_putchar(s[i], 1);
	  i++;
	}
    }
}

void	init_char(char *s, char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '\\' && str[i] != '\0')
	{
	  i++;
	  if (str[i] != s[i])
	    my_putchar(s[(int)str[i++]], 1);
	  else if (str[i] == 'c')
	    return;
	  else
	    {
	      my_putchar('\\', 1);
	      my_putchar(str[i], 1);
	    }
	}
      else if (str[i] == '"')
	i++;
      else
	my_putchar(str[i++], 1);
    }
}

void	my_print_backslash(char *str)
{
  char	s[128];
  int	i;

  i = 0;
  while (i <= 127)
    {
      s[i] = i;
      i++;
    }
  s['\\'] = '\\';
  s['a'] = '\a';
  s['b'] = '\b';
  s['f'] = '\f';
  s['n'] = '\n';
  s['r'] = '\r';
  s['t'] = '\t';
  s['v'] = '\v';
  init_char(s, str);
}
