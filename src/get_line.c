/*
** get_line.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sun May 24 14:14:00 2015 grandr_r
** Last update Sun May 24 14:14:15 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

char	*get_line(int fd)
{
  char	buffer[2];
  char	*str;
  int	ret;

  if ((str = malloc(sizeof(char) * 2)) == NULL)
    return (NULL);
  str[0] = '\0';
  buffer[0] = 0;
  while (buffer[0] != '\n')
    {
      if ((ret = read(fd, buffer, 1)) <= 0)
        return (NULL);
      buffer[ret] = '\0';
      str = my_realloc(str, buffer);
    }
  if (str[0] == '\0')
    {
      free(str);
      return (NULL);
    }
  str[my_strlen(str) - 1] = 0;
  return (str);
}
