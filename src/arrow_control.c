/*
** arrow_control.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/src
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Mon Apr 13 19:12:47 2015 remy grandry
** Last update Sun May 24 17:31:16 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

char	*left_arrow(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  int	tmp;

  tmp = my_strlen(s) - 1;
  if (sh2->pos < tmp)
    {
      my_putchar(s[sh2->pos + 1], 1);
      sh2->pos++;
    }
  return (s);
}

char    *down_arrow(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  if (sh[0]->next->buff != NULL)
    {
      disp_cmd(sh[0]->next->buff, s, sh2);
      sh[0] = sh[0]->next;
      sh2->pos = my_strlen(sh[0]->buff) - 1;
      free(s);
      return (my_strdup(sh[0]->buff));
    }
  return (NULL);
}

char    *up_arrow(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  if (sh[0]->prev->buff != NULL)
    {
      disp_cmd(sh[0]->prev->buff, s, sh2);
      sh[0] = sh[0]->prev;
      sh2->pos = my_strlen(sh[0]->buff) - 1;
      free(s);
      return (my_strdup(sh[0]->buff));
    }
  return (NULL);
}

char    *right_arrow(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  if (sh2->pos > -1)
    {
      my_putchar('\b', 1);
      sh2->pos--;
    }
  return (s);
}

char	*arrow_control(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  if (buf[0] == 27 && buf[1] == 91 && buf[2] == 65)
    return (up_arrow(s, buf, sh, sh2));
  else if (buf[0] == 27 && buf[1] == 91 && buf[2] == 66)
    return (down_arrow(s, buf, sh, sh2));
  else if (buf[0] == 27 && buf[1] == 91 && buf[2] == 68)
    return (right_arrow(s, buf, sh, sh2));
  else if (buf[0] == 27 && buf[1] == 91 && buf[2] == 67)
    return (left_arrow(s, buf, sh, sh2));
  else if (buf[0] == 12)
    return (my_clrscreen(s, buf));
  else if (buf[0] == 27 && buf[1] == 91 && buf[2] == 51 && buf[3] == 126)
    return (delete_key(s, sh2));
  else if (buf[0] == 9)
    return (tab_key(s, sh2, 0, my_realloc(my_strdup(func_name(s, 1)), "*")));
  return (NULL);
}
