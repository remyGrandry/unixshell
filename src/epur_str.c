/*
** epur_str.c for lib in /Users/grandr/Epitech/Unix_Prog/PSU_2014_minishell1/lib/my
**
** Made by Remy GRANDRY
** Login   <grandr@epitech.net>
**
** Started on  Mon Jan 26 09:04:05 2015 Remy GRANDRY
** Last update Sat May 23 17:20:39 2015 grandr_r
*/

#include <stdlib.h>
#include "my.h"

void	remove_letter(char *s, int pos)
{
  while (s[pos] != '\0')
    {
      s[pos] = s[pos + 1];
      pos++;
    }
}

int	only_char(char *str, char sep)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] != sep)
	return (0);
      i++;
    }
  return (1);
}

int	only_chars(char *s, char sep, char sep2, t_sh *sh)
{
  int	i;

  i = 0;
  if (s == NULL)
    return (0);
  sh->epur_cmd = my_str_to_wordtab(s, 0, ';');
  while (s[i])
    {
      if (s[i] != sep && s[i] != sep2)
	return (1);
      i++;
    }
  my_free_wordtab(sh->epur_cmd);
  return (0);
}

char	*epur_str(char *str, char sep)
{
  int	c;
  int	c2;
  char	*dest;

  c = 0;
  c2 = 0;
  if (str == NULL || only_char(str, sep) == 1)
    return (NULL);
  if ((dest = malloc(sizeof(char) * (my_strlen(str) + 1))) == NULL)
    exit(-1);
  while (str[c] == sep || str[c] == '\t')
    c++;
  while (c < (my_strlen(str) - 1))
    {
      if ((str[c] != sep || str[c + 1] != sep) &&
	  (str[c] != '\t' || str[c + 1] != '\t'))
	dest[c2++] = str[c];
      c++;
    }
  if (str[c] != sep && str[c] != '\t')
    dest[c2++] = str[c];
  dest[c2] = 0;
  return (dest);
}
