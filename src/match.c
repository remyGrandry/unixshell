/*
** match.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/src
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 15:24:28 2015 remy grandry
** Last update Fri May 22 18:11:07 2015 grandr_r
*/

#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <my.h>

int		test_dir(char *path)
{
  struct stat 	st_buf;

  if (stat(path, &st_buf) != 0)
    return (-1);
  if (S_ISDIR(st_buf.st_mode))
    return (0);
  else
    return (1);
  return (-1);
}

  int	char_is_instr(char c, char *s)
  {
    int	i;

    i = 0;
    if (s == NULL)
      return (0);
    while (s[i])
      {
	if (c == s[i])
	  return (1);
	i++;
      }
    return (0);
  }

  int	nmatch(char *s1, char *s2)
  {
    if (*s1 == '\0' && *s2 == '\0')
      return (1);
    else if (*s1 == *s2 && *s1 != '*')
      return (nmatch(s1 + 1, s2 + 1) > 0 ? 1 : 0);
    else if (*s1 == '*' && *s2 == '*')
      return (nmatch (s1 + 1, s2) > 0 ? 1 : 0);
    else if (*s2 == '*')
      {
	if (*s1 == '\0')
	  return (nmatch(s1, s2 + 1) > 0 ? 1 : 0);
	else
	  return (nmatch(s1, s2 + 1) + nmatch(s1 + 1, s2) > 0 ? 1 : 0);
      }
    else
      return (0);
  }

  int	match(char *s1, char *s2)
  {
    if (s1 == NULL || s2 == NULL)
      return (0);
    return (nmatch(s1, s2));
  }
