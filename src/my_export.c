/*
** my_setenv.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh/src
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Tue May 12 14:10:56 2015 grandr_r
** Last update Sat May 23 20:20:51 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

int	export_opt(char *all, t_sh *sh, char *name, int *test)
{
  int	i;

  i = 0;
  if (sh->my_env == NULL || sh->my_env[0] == NULL)
    {
      sh->new[0] = my_strdup(all);
      *test = 1;
      return (1);
    }
  while (sh->my_env[i])
    {
      if (my_strncmp(sh->my_env[i], name, my_strlen(name)) == 0)
	{
	  sh->new[i] = my_strdup(all);
	  *test = 1;
	}
      else
	sh->new[i] = my_strdup(sh->my_env[i]);
      i++;
    }
  return (i);
}

int	my_export2(char *all, t_sh *sh, char *name)
{
  int	ret;
  int	test;

  test = 0;
  if ((sh->new = malloc((sizeof(char *) * (my_arrlen(sh->my_env)
					   + 3)))) == NULL)
    exit(-1);
  ret = export_opt(all, sh, name, &test);
  sh->new[ret] = NULL;
  if (test == 0)
    sh->new[ret] = my_strdup(all);
  else if (test == 1)
    sh->new[ret] = NULL;
  sh->new[ret + 1] = NULL;
  my_free_wordtab(sh->my_env);
  my_env_copy(sh, sh->new);
  return (1);
}

int	my_export(char *str, t_sh *sh)
{
  char	**tmp;
  char	**tmp2;

  tmp2 = NULL;
  tmp = NULL;
  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match(tmp[0], "export") == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (char_is_instr('=', str) == 0)
    {
      my_free_wordtab(tmp);
      return (1);
    }
  tmp2 = my_str_to_wordtab(tmp[1], 0, '=');
  my_free_wordtab(sh->new);
  my_export2(tmp[1], sh, tmp2[0]);
  my_free_wordtab(tmp);
  my_free_wordtab(tmp2);
  return (1);
}
