/*
** my_strcat.c for my_strcat in /Users/grandr/Epitech/lib/my
**
** Made by Remy GRANDRY
** Login   <grandr@epitech.net>
**
** Started on  Thu Jan 15 10:07:23 2015 Remy GRANDRY
** Last update Sat May 23 17:27:17 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

char    *my_strcat3(char *dest, char *src)
{
  int   i;
  int   j;

  j = 0;
  i = my_strlen(dest);
  while (src[j])
    dest[i++] = src[j++];
  return (dest);
}
