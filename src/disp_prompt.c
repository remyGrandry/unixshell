/*
** disp_prompt.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Tue May 12 10:45:39 2015 grandr_r
** Last update Sun May 24 21:03:06 2015 grandr_r
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <my.h>
#include <stw.h>

void	create_prompt(char **env, int n, char *s)
{
  char	*s2;
  char	buff[1024];
  int	fd;

  if ((fd = open("/tmp/.42sh", O_RDWR | O_TRUNC | O_CREAT, 0666)) == -1)
    return;
  if ((s = my_strdup(my_getenv(env, "USER="))) != NULL)
    write(fd, s, my_strlen(s));
  else
    write(fd, "root", my_strlen("root"));
  getcwd(buff, sizeof(buff));
  s2 = my_strdup(my_getenv(env, "HOME="));
  s2 = my_realloc(s2, "*");
  write(fd, "@:", 2);
  if (match(buff, s2) == 1)
    {
      n += my_strlen(s2) - 1;
      write(fd, "~", 1);
    }
  write(fd, &buff[n], my_strlen(buff) - n);
  write(fd, " \n", 2);
  free(s2);
  free(s);
  close(fd);
}

int	disp_prompt_safe(t_sh *sh)
{
  int   fd;
  int	fd2;
  char  *s;

  if ((fd = open("/tmp/.42sh", O_RDONLY)) == -1 && write(1, PROMPT, 2))
    return (1);
  if ((fd2 = open("/tmp/.42safe", O_RDWR | O_CREAT, 0666)) != -1)
    close(fd2);
  my_free_wordtab(sh->cur_prompt);
  s = get_line(fd);
  sh->cur_prompt = my_str_to_wordtab(s, 0, ':');
  if (my_getenv(sh->my_env, "?=") != NULL
      && my_getenv(sh->my_env, "?=")[0] != '0')
    my_putstr("\x1B[31mx \x1B[0m", 1);
  else
    my_putstr("\x1B[32mv \x1B[0m", 1);
  my_putstr(sh->cur_prompt[0], 1);
  my_putstr("\x1B[33;7m> ", 1);
  my_putstr(sh->cur_prompt[1], 1);
  my_putstr("\x1B[0m", 1);
  my_putstr("\x1B[33m> \x1B[0m", 1);
  free(s);
  close(fd);
  return (0);
}

void	disp_prompt_ctrlc_safe()
{
  int   fd;
  char  *s;
  char	**wt;

  if ((fd = open("/tmp/.42sh", O_RDONLY)) == -1)
    {
      my_putstr(PROMPT, 1);
      return;
    }
  s = get_line(fd);
  wt = my_str_to_wordtab(s, 0, ':');
  my_putstr("\n\x1B[31mx \x1B[0m", 1);
  my_putstr(wt[0], 1);
  my_putstr("\x1B[33;7m> ", 1);
  my_putstr(wt[1], 1);
  my_putstr("\x1B[0m", 1);
  my_putstr("\x1B[33m> \x1B[0m", 1);
  my_free_wordtab(wt);
  free(s);
  close(fd);
}

void	disp_prompt_ctrlc()
{
  int   fd;
  char  *s;
  char	**wt;

  if ((fd = open("/tmp/.42sh", O_RDONLY)) == -1)
    {
      my_putstr(PROMPT, 1);
      return;
    }
  if (access("/tmp/.42safe", F_OK) == 0)
    {
      disp_prompt_ctrlc_safe();
      return;
    }
  s = get_line(fd);
  wt = my_str_to_wordtab(s, 0, ':');
  my_putstr("\n\x1B[31m✘ \x1B[0m", 1);
  my_putstr(wt[0], 1);
  my_putstr("\x1B[33;7m ", 1);
  my_putstr(wt[1], 1);
  my_putstr("\x1B[0m", 1);
  my_putstr("\x1B[33m \x1B[0m", 1);
  my_free_wordtab(wt);
  free(s);
  close(fd);
}

int	disp_prompt(t_sh *sh)
{
  int	fd;
  char	*s;

  create_prompt(sh->my_env, 0, NULL);
  if (sh->safe == 1)
    return (disp_prompt_safe(sh));
  if ((fd = open("/tmp/.42sh", O_RDONLY)) == -1 && write(1, PROMPT, 2))
    return (1);
  my_free_wordtab(sh->cur_prompt);
  s = get_line(fd);
  sh->cur_prompt = my_str_to_wordtab(s, 0, ':');
  if (my_getenv(sh->my_env, "?=") != NULL
      && my_getenv(sh->my_env, "?=")[0] != '0')
    my_putstr("\x1B[31m✘ \x1B[0m", 1);
  else
    my_putstr("\x1B[32m✔ \x1B[0m", 1);
  my_putstr(sh->cur_prompt[0], 1);
  my_putstr("\x1B[33;7m ", 1);
  my_putstr(sh->cur_prompt[1], 1);
  my_putstr("\x1B[0m", 1);
  my_putstr("\x1B[33m \x1B[0m", 1);
  free(s);
  close(fd);
  return (0);
}
