/*
** error.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May  6 16:00:46 2015 grandr_r
** Last update Fri May 22 19:08:43 2015 grandr_r
*/

#include <string.h>
#include <errno.h>
#include <my.h>

int	function_error(char *func, char *param)
{
  my_putstr(func, 2);
  my_putstr(strerror(errno), 2);
  my_putstr(" : ", 2);
  my_putstr(param, 2);
  my_putchar('\n', 2);
  return (1);
}

int	function_error2(char *func, char *param, char **tmp, char **tmp2)
{
  my_putstr(func, 2);
  my_putstr(strerror(errno), 2);
  my_putstr(" : ", 2);
  my_putstr(param, 2);
  my_putchar('\n', 2);
  my_free_wordtab3(tmp, tmp2);
  return (1);
}

int	exec_error(char *func)
{
  my_putstr("-42sh: ", 2);
  my_putstr(func, 2);
  my_putstr(": ", 2);
  my_putstr(strerror(errno), 2);
  my_putchar('\n', 1);
  return (-1);
}

int	return_error(char *s, char **tmp)
{
  my_putstr(s, 2);
  my_free_wordtab(tmp);
  return (1);
}

int	command_error(char *s, t_sh *sh, char **tmp)
{
  my_putstr("42sh: ", 2);
  my_putstr(s, 2);
  my_putstr(" : command not found\n", 2);
  add_to_env(1, sh);
  my_free_wordtab(tmp);
  return (0);
}
