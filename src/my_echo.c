/*
** my_echo.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sat May  9 10:16:52 2015 grandr_r
** Last update Tue May 12 10:19:25 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>

void	verif_option(char *opt, int *newline, int *backslash, int *lentoprint)
{
  int	valid_opt;

  valid_opt = 0;
  *newline = 1;
  if (char_is_instr('n', opt) == 1)
    {
      valid_opt++;
      *newline = 0;
    }
  if (char_is_instr('e', opt) == 1)
    {
      valid_opt++;
      *backslash = 1;
    }
  if (char_is_instr('E', opt) == 1)
    valid_opt++;
  if (valid_opt == (my_strlen(opt) - 1))
    *lentoprint = my_strlen("echo ") + my_strlen(opt) + 1;
  else
    *lentoprint = my_strlen("echo ");
}

int 	echo_opt(char **tmp, char *str, int *newline, t_sh *sh)
{
  int	backslash;
  int	lentoprint;

  lentoprint = 5;
  backslash = 0;
  if (tmp[1] == NULL)
    {
      my_free_wordtab(tmp);
      my_putchar('\n', 1);
      return (1);
    }
  if (tmp[1][0] == '-')
    verif_option(tmp[1], newline, &backslash, &lentoprint);
  if (backslash == 0)
    my_print_echo(str + lentoprint, sh->my_env);
  else
    my_print_backslash(str + lentoprint);
  return (0);
}

int	my_echo(t_42 *cmd, t_sh *sh, char *str)
{
  char	**tmp;
  int	newline;

  newline = 1;
  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match("echo", tmp[0]) == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (echo_opt(tmp, str, &newline, sh) == 1)
    return (1);
  if (newline == 1)
    my_putchar('\n', 1);
  my_free_wordtab(tmp);
  return (1);
}
