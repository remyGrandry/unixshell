/*
** my_getenv.c for minishell2 in /Users/grandr/Epitech/Unix_Prog/PSU_2014_minishell2
**
** Made by Remy GRANDRY
** Login   <grandr@epitech.net>
**
** Started on  Thu Mar  5 12:21:16 2015 Remy GRANDRY
** Last update Wed May 13 18:28:25 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

char	*my_getenv(char **env, char *var)
{
  int	i;

  i = 0;
  if (env == NULL || env[0] == NULL || var == NULL)
    return (NULL);
  while (env[i] != NULL)
    {
      if (my_strncmp(env[i], var, my_strlen(var)) == 0)
	return (&env[i][my_strlen(var)]);
      i++;
    }
  return (NULL);
}
