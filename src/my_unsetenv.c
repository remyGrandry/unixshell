/*
** my_unsetenv.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Wed May 13 10:36:17 2015 grandr_r
** Last update Sat May 23 20:09:45 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

void	my_unsetenv2(char *n, t_sh *sh, int i, int j)
{
  my_free_wordtab(sh->new);
  if ((sh->new = malloc(sizeof(char *) * (my_arrlen(sh->my_env) + 2))) == NULL)
    exit(-1);
  while (sh->my_env[i])
    {
      if (my_strncmp(sh->my_env[i], n, my_strlen(n)) == 0)
        i++;
      else
	sh->new[j++] = my_strdup(sh->my_env[i++]);
    }
  sh->new[j] = NULL;
  my_free_wordtab(sh->my_env);
  my_env_copy(sh, sh->new);
}

int	my_unsetenv(char *str, t_sh *sh)
{
  char	**tmp;

  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match(tmp[0], "unsetenv") == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (tmp[1] == NULL)
    {
      my_free_wordtab(tmp);
      return (1);
    }
  if (my_strncmp(tmp[1], "*", 1) == 0)
    {
      my_free_wordtab(tmp);
      my_free_wordtab(sh->my_env);
      sh->my_env = NULL;
      return (1);
    }
  my_unsetenv2(tmp[1], sh, 0, 0);
  my_free_wordtab(tmp);
  return (1);
}
