/*
** get_next_line.c for get_next_line in /Users/grandr/Epitech/Unix_Prog/PSU_2014_minishell1
**
** Made by Remy GRANDRY
** Login   <grandr@epitech.net>
**
** Started on  Thu Jan 15 10:50:56 2015 Remy GRANDRY
** Last update Sun May 24 20:57:31 2015 grandr_r
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <my.h>

char	*my_reallocc(char *str, char c, int pos)
{
  char  *new;
  int   i;
  int	j;

  i = 0;
  j = 0;
  new = malloc(sizeof(*new) * (my_strlen(str) + 2));
  if (new == NULL)
    return (NULL);
  while (str[i - j] != '\0')
    {
      if (i == pos)
	{
	  new[i] = c;
	  j = 1;
	}
      else
	new[i] = str[i - j];
      i = i + 1;
    }
  if (j == 0)
    new[i++] = c;
  new[i] = '\0';
  free(str);
  return (new);
}

char    *my_realloc(char *str, char *buffer)
{
  char  *new;
  int   i;
  int   z;

  i = 0;
  z = 0;
  if (str == NULL || buffer == NULL)
    return (NULL);
  new = malloc(sizeof(*new) * (my_strlen(str) + my_strlen(buffer) + 1));
  if (new == NULL || buffer == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      new[i] = str[i];
      i = i + 1;
    }
  while (buffer[z] != '\0')
    new[i++] = buffer[z++];
  new[i] = '\0';
  free(str);
  return (new);
}

void	disp_cmd(char *cmd, char *s, t_sh *sh)
{
  int	i;
  int	n;

  i = -1;
  if (sh->cur_prompt == NULL || sh->cur_prompt[0] == NULL)
    n = 2;
  else
    n = 6 + my_strlen(sh->cur_prompt[0]) + my_strlen(sh->cur_prompt[1]);
  my_putchar('\r', 1);
  while (++i < my_strlen(s) + n)
    my_putchar(' ', 1);
  my_putchar('\r', 1);
  disp_prompt(sh);
  write(1, cmd, my_strlen(cmd));
}

char	*my_writer(char *s, char *buf, t_42 **sh, t_sh *sh2)
{
  char	*tmp;
  int	len;

  if (buf == NULL)
    exit(-1);
  if (buf[0] == 4 || buf[0] == 0)
    exit_ctrld(sh[0], sh2, s);
  if ((tmp = arrow_control(s, buf, sh, sh2)) != NULL)
    return (tmp);
  if (buf[0] == 127)
    return (backspace(s, sh2));
  if (char_is_printable(buf[0]) == 0)
    return (s);
  tmp = my_strdup(s);
  len = my_strlen(s);
  write(1, buf, 1);
  s = my_reallocc(s, buf[0], sh2->pos + 1);
  disp_cmd(s, tmp, sh2);
  while (--len > sh2->pos)
    my_putchar('\b', 1);
  sh2->pos++;
  free(tmp);
  return (s);
}

char    	*get_next_line(int fd, t_42 *sh, t_sh *sh2)
{
  char		*s;
  char		buf[5];
  static int	ret = 0;

  if (sh2->termcap == 0)
    return (get_line(fd));
  if ((s = malloc(1)) == NULL)
    exit(-1);
  s[0] = 0;
  buf[0] = 0;
  while (buf[0] != '\n')
    {
      if ((ret = read(fd, buf, 4)) <= 0)
	exit(-1);
      if ((s = my_writer(s, buf, &sh, sh2)) == NULL)
	exit(-1);
    }
  write(1, "\n", 1);
  sh2->pos = -1;
  return (s);
}
