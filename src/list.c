/*
** list.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 15:44:43 2015 remy grandry
** Last update Wed May 20 11:11:48 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>

void	display_list(t_42 *list)
{
  t_42	*root;
  int	i;

  i = 0;
  root = list->next;
  while (root != list)
    {
      if (root->buff != NULL)
	printf("%d %s\n", i, root->buff);
      root = root->next;
      i++;
    }
}

void	remove_elem(t_42 *root)
{
  free(root->buff);
  root->prev->next = root->next;
  root->next->prev = root->prev;
  free(root);
}

void	del_first_elem(t_42 *root)
{
  root->prev->next = root->next;
  root->next->prev = root->prev;
  free(root);
}

void	add_next(t_42 *root, char *str)
{
  t_42	*new;

  if ((new = malloc(sizeof(*new))) == NULL)
    exit(1);
  new->buff = str;
  new->prev = root;
  new->next = root->next;
  root->next->prev = new;
  root->next = new;
}

void	add_prev(t_42 *root, char *str)
{
  t_42	*new;

  if ((new = malloc(sizeof(*new))) == NULL)
    exit(1);
  new->buff = str;
  new->prev = root->prev;
  new->next = root;
  root->prev->next = new;
  root->prev = new;
}
