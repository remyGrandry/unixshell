/*
** my_strdup.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Mon May  4 16:17:09 2015 grandr_r
** Last update Sat May 23 17:36:34 2015 grandr_r
*/

#include <stdlib.h>
#include <my.h>

char    *my_strdup(char *s)
{
  char  *ret;
  int   i;

  i = 0;
  if (s == NULL)
    return (NULL);
  if ((ret = malloc(sizeof(char) * (my_strlen(s) + 1))) == NULL)
    exit(-1);
  while (s[i])
    {
      ret[i] = s[i];
      i++;
    }
  ret[i] = '\0';
  return (ret);
}

char    *my_strdup2(char *s, char *s2)
{
  char  *ret;
  int   i;

  i = 0;
  if (s == NULL)
    return (NULL);
  if ((ret = malloc(sizeof(char) * (my_strlen(s) + 1))) == NULL)
    exit(-1);
  while (s[i])
    {
      ret[i] = s[i];
      i++;
    }
  ret[i] = '\0';
  free(s2);
  return (ret);
}
