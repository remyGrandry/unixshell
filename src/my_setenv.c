/*
** my_setenv.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh/src
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Tue May 12 14:10:56 2015 grandr_r
** Last update Sun May 24 09:41:14 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <my.h>

char	*parse_env(char *cmd, char *name, char *val)
{
  char	*ret;

  ret = my_strdup(cmd);
  ret = my_realloc(ret, name);
  ret = my_realloc(ret, "=");
  if (val != NULL)
    ret = my_realloc(ret, val);
  return (ret);
}

int	my_setenv(char *str, t_sh *sh)
{
  char	**tmp;
  char	*s;

  tmp = NULL;
  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match(tmp[0], "setenv") == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (tmp[1] == NULL)
    {
      my_env_print(sh->my_env);
      return (1);
    }
  s = parse_env("export ", tmp[1], tmp[2]);
  my_export(s, sh);
  free(s);
  my_free_wordtab(tmp);
  return (1);
}
