/*
** my_exit.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 15:58:06 2015 remy grandry
** Last update Sun May 24 21:57:25 2015 grandr_r
*/

#include <stdlib.h>
#include <stdio.h>
#include <my.h>

void	free_all(t_sh *sh2, char *s, t_42 *sh)
{
  t_42	*root;
  char	**tmp;

  root = sh->prev;
  tmp = my_str_to_wordtab("/bin/rm -f /tmp/.42sh /tmp/.42safe", 0, ' ');
  shexec("/bin/rm", tmp, sh2->my_env, sh2);
  my_free_wordtab(tmp);
  while (root != sh)
    {
      root = root->prev;
      remove_elem(root->next);
    }
  remove_elem(root);
  my_free_wordtab(sh2->my_path);
  my_free_wordtab(sh2->msg);
  if (sh2->my_env != NULL)
    my_free_wordtab(sh2->my_env);
  my_free_wordtab(sh2->new);
  free(sh2->oldpwd);
  my_free_wordtab(sh2->cur_prompt);
  free(sh2->current_pwd);
  if (s != NULL)
    free(s);
}

int	exit_ctrld(t_42 *sh, t_sh *sh2, char *s)
{
  my_putchar('\n', 1);
  canon_off(sh2);
  free_all(sh2, s, sh);
  free(sh2);
  exit(0);
}

int    my_exit(t_42 *sh, t_sh *sh2, char *s)
{
  char	**tmp;
  int	value;

  value = 0;
  tmp = my_str_to_wordtab(s, 0, ' ');
  if (tmp[1] != NULL)
    value = my_getnbr(tmp[1]);
  if (match(tmp[0], "exit") == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  my_free_wordtab(tmp);
  canon_off(sh2);
  my_free_wordtab(sh2->all_cmd);
  my_free_wordtab(sh2->epur_cmd);
  free_all(sh2, NULL, sh);
  free(sh2);
  exit(value);
}
