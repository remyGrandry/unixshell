/*
** other_key.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh/src
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Apr 14 19:49:49 2015 remy grandry
** Last update Sun May 24 17:32:04 2015 grandr_r
*/

#include <string.h>
#include <stdlib.h>
#include <glob.h>
#include <stdio.h>
#include <my.h>

char	*func_name(char *s, int mode)
{
  int	i;

  i = my_strlen(s);
  while (i >= 0 && (s[i] != ' ' && s[i] != '\t'))
    i--;
  if (mode == 0)
    {
      s[i + 1] = '\0';
      return (s);
    }
  return (&s[i + 1]);
}

char		*tab_key(char *s, t_sh *sh, int i, char *s2)
{
  glob_t	g_buff;

  if (glob(s2, 0, NULL, &g_buff) != 0)
    {
      free(s2);
      return (s);
    }
  if (my_arrlen(g_buff.gl_pathv) > 1)
    {
      my_putchar('\n', 1);
      while (g_buff.gl_pathv[i] != NULL)
	{
	  my_putstr(g_buff.gl_pathv[i++], 1);
	  my_putchar('\n', 1);
	}
    }
  else if (my_arrlen(g_buff.gl_pathv) == 1)
    {
      s = my_realloc(my_strdup2(func_name(s, 0), s), g_buff.gl_pathv[0]);
      sh->pos = my_strlen(s);
    }
  disp_cmd(s, s, sh);
  free(s2);
  globfree(&g_buff);
  return (s);
}

char	*my_clrscreen(char *s, char *tmp)
{
  my_putstr("\033[2J\033[0;0f", 1);
  my_putstr("\033[0;0f", 1);
  disp_prompt_ctrlc();
  my_putstr(s, 1);
  return (NULL);
}

char	*delete_key(char *s, t_sh *sh2)
{
  char  *tmp;
  int   len;

  len = my_strlen(s);
  tmp = my_strdup(s);
  if (sh2->pos < my_strlen(s))
    {
      remove_letter(s, sh2->pos + 1);
      disp_cmd(s, tmp, sh2);
      while (--len > sh2->pos + 1)
	my_putchar('\b', 1);
    }
  free(tmp);
  return (s);
}

char    *backspace(char *s, t_sh *sh2)
{
  char  *tmp;
  int   len;

  len = my_strlen(s) - 1;
  tmp = my_strdup(s);
  if (sh2->pos >= 0)
    {
      remove_letter(s, sh2->pos);
      sh2->pos--;
      disp_cmd(s, tmp, sh2);
      while (--len > sh2->pos)
	my_putchar('\b', 1);
    }
  free(tmp);
  return (s);
}
