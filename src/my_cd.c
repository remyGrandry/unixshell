/*
** my_cd.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Thu May  7 13:43:14 2015 grandr_r
** Last update Wed Jun  3 08:51:39 2015 grandr_r
*/

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <my.h>

int	cd_less(t_42 *cmd, t_sh *sh, char *str, char **tmp)
{
  char	*tmp2;

  if (sh->oldpwd == NULL)
    {
      my_putstr("Error: oldpwd is not set\n", 1);
      my_free_wordtab(tmp);
      return (1);
    }
  if (chdir(sh->oldpwd) == -1)
    my_putstr(strerror(errno), 2);
  else
    {
      tmp2 = my_strdup(sh->current_pwd);
      free(sh->current_pwd);
      sh->current_pwd = my_strdup(sh->oldpwd);
      free(sh->oldpwd);
      sh->oldpwd = my_strdup(tmp2);
      free(tmp2);
      my_putstr(sh->current_pwd, 1);
      my_putchar('\n', 1);
    }
  my_free_wordtab(tmp);
  return (1);
}

int	cd_option(t_42 *cmd, t_sh *sh, char *str, char **tmp)
{
  char	buff[1024];

  if (tmp[1] == NULL || match(tmp[1], "~") == 1)
    {
      free(sh->current_pwd);
      sh->current_pwd = my_strdup(getcwd(buff, sizeof(buff)));
      if (my_getenv(sh->my_env, "HOME=") != NULL)
	{
	  if (chdir(my_getenv(sh->my_env, "HOME=")) == -1)
	    my_putstr(strerror(errno), 2);
	  else
	    {
	      free(sh->oldpwd);
	      sh->oldpwd = my_strdup(sh->current_pwd);
	      free(sh->current_pwd);
	      sh->current_pwd = my_strdup(getcwd(buff, sizeof(buff)));
	    }
	}
      my_free_wordtab(tmp);
      return (1);
    }
  else if (match(tmp[1], "-") == 1)
    return (cd_less(cmd, sh, str, tmp));
  return (0);
}

int	my_cd(t_42 *cmd, t_sh *sh, char *str)
{
  char	**tmp;
  char	buff[1024];

  tmp = my_str_to_wordtab(str, 0, ' ');
  if (match("cd", tmp[0]) == 0)
    {
      my_free_wordtab(tmp);
      return (0);
    }
  if (cd_option(cmd, sh, str, tmp) == 1)
    return (1);
  free(sh->current_pwd);
  sh->current_pwd = my_strdup(getcwd(buff, sizeof(buff)));
  if (chdir(tmp[1]) == -1)
    function_error("cd: ", tmp[1]);
  else
    {
      free(sh->oldpwd);
      sh->oldpwd = my_strdup(sh->current_pwd);
      free(sh->current_pwd);
      sh->current_pwd = my_strdup(getcwd(buff, sizeof(buff)));
    }
  my_free_wordtab(tmp);
  return (1);
}
