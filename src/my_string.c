/*
** my_string.c for 42sh in /home/grandr_r/Unix_Prog/PSU_2014_42sh
**
** Made by remy grandry
** Login   <grandr_r@epitech.net>
**
** Started on  Tue Mar 10 16:07:22 2015 remy grandry
** Last update Sat May 23 17:26:16 2015 grandr_r
*/

#include <stdlib.h>
#include <unistd.h>

int     my_strcmp(char *s1, char *s2)
{
  int   i;

  i = 0;
  if (s1 == NULL || s2 == NULL)
    return (-1);
  while (s1[i])
    {
      if (s1[i] < s2[i])
	return (-1);
      if (s1[i] > s2[i])
	return (1);
      i++;
    }
  if (s1[i] < s2[i])
    return (-1);
  if (s1[i] > s2[i])
    return (1);
  return (0);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  if (str == NULL)
    return (0);
  while (str[i])
    i++;
  return (i);
}

void	my_putchar(char c, int out)
{
  write(out, &c, 1);
}

void	my_putstr(char *str, int out)
{
  if (str == NULL)
    write(out, "(null)", 6);
  else
    write(out, str, my_strlen(str));
}
