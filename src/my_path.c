/*
** my_path.c for 42sh in /home/grandr_r/Epitech/Unix_Prog/PSU_2014_42sh
**
** Made by grandr_r
** Login   <grandr_r@epitech.net>
**
** Started on  Sat May  9 10:00:15 2015 grandr_r
** Last update Wed May 13 18:17:32 2015 grandr_r
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <my.h>

void	my_uptdate_path(t_sh *sh)
{
  char	*s;

  s = NULL;
  if (sh->my_env != NULL)
    {
      my_free_wordtab(sh->my_path);
      s = my_strdup(my_getenv(sh->my_env, "PATH="));
      if (s != NULL)
	sh->my_path = my_str_to_wordtab(s, 0, ':');
      else
	sh->my_path = NULL;
      free(s);
    }
}

char	*fill_path(t_sh *sh, char *name)
{
  int	i;
  char	*s;

  i = 0;
  if (sh->my_path == NULL)
    return (NULL);
  while (sh->my_path[i])
    {
      s = my_strdup(sh->my_path[i]);
      s = my_realloc(s, "/");
      s = my_realloc(s, name);
      if (access(s, F_OK) == 0)
	return (s);
      free(s);
      i++;
    }
  return (NULL);
}
